# INTERVALOMETER #

This device works by controlling a bluetooth HID remote shutter with a transistor, pinging it from time to time so the camera would take frames for the time lapse. It also uses GUI to see the status of how many frames are left in a Windows Forms.

### How does this work? ###

* Arduino calculates the interval needed for the time lapse, waiting for a command with the params on the serial in the beginning, and then clicks the shutter right away at every needed frame( **C++** ).
* *Optionally*, its state could be shown on GUI( **C#** ).