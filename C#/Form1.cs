﻿using System;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Arduino_Control
{
    public partial class Form1 : Form
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();
        public Form1()
        {
            AllocConsole();
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            Program.port.DataReceived += new
              SerialDataReceivedEventHandler(port_DataReceived);

            Program.port.Open();
        }

        private void port_DataReceived(object sender,
          SerialDataReceivedEventArgs e)
        {
            var x = Program.port.ReadExisting();
            if (x.StartsWith("/xconf"))
            {
                char[] charSeparators = new char[] { '/' };
                var config = x.Substring(5).Split(charSeparators);
                label9.Text = config[0];
                label11.Text = config[1];
                label13.Text = config[2];
            }
            else if (x.Contains("/xend"))
            {
                MessageBox.Show("Time Lapse Done!");
                Application.Exit();
            }
            else
            {
                label1.Text = x;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            label1.Visible = true;
            label2.Visible = true;
            label8.Visible = true;
            label9.Visible = true;
            label10.Visible = true;
            label11.Visible = true;
            label12.Visible = true;
            label13.Visible = true;

            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            textBox4.Enabled = false;
            textBox5.Enabled = false;

            Program.port.WriteLine("/xinit/" + textBox1.Text + "/" + textBox2.Text + "/" + textBox3.Text + "/" + textBox4.Text + "/" + textBox5.Text + "/");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int tmp;
            if (!int.TryParse(textBox1.Text, out tmp))
            {
                textBox1.Text = "";
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            int tmp;
            if (!int.TryParse(textBox2.Text, out tmp))
            {
                textBox2.Text = "";
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            int tmp;
            if (!int.TryParse(textBox3.Text, out tmp))
            {
                textBox3.Text = "";
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            int tmp;
            if (!int.TryParse(textBox4.Text, out tmp))
            {
                textBox4.Text = "";
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            int tmp;
            if (!int.TryParse(textBox5.Text, out tmp))
            {
                textBox5.Text = "";
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
