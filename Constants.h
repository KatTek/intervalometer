typedef unsigned int uint;

constexpr uint MAX_SERIAL_CHARS = 50;
constexpr uint SHUTTER_PIN = 8;
constexpr uint BAUD_RATE = 128000;
constexpr const char *DATA_INITIALIZATION = "/xinit";
constexpr const char *DATA_CONFIG = "/xconf";
constexpr const char *DATA_END = "/xend";