﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace Arduino_Control
{
    public partial class Form2 : Form
    {
        bool[] boxSelected = new bool[2];
        public Form2()
        {
            InitializeComponent();
            button1.Enabled = false;
            var ports = SerialPort.GetPortNames();
            if(ports.Length>0)
            {
                foreach (string port in ports)
                {
                    comboBox2.Items.Add(port);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.port = new SerialPort(comboBox2.SelectedItem.ToString(), Convert.ToInt32( comboBox1.SelectedItem), Parity.None, 8, StopBits.One);
            
            var form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            boxSelected[0] = true;
            if( boxSelected[1] == true)
            {
                button1.Enabled = true;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            boxSelected[1] = true;
            if (boxSelected[0] == true)
            {
                button1.Enabled = true;
            }
        }
    }
}
