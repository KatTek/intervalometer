#include "Constants.h"

uint secondsPerFrame = 0, timeTotal = 0, timeFinal = 0, fps = 0, shutterClickSpeed = 0, shutterSpeed = 0, frames = 0, totalFrames = 0;

bool start = false, ended = false;

void setup()
{
    pinMode(SHUTTER_PIN, OUTPUT);
    digitalWrite(SHUTTER_PIN, LOW);
    Serial.begin(BAUD_RATE);
}

void onSerialProcessData(char data[])
{
    if (strlen(data) > 0)
    {
        if (strstr(data, DATA_INITIALIZATION))
        {
            char tmp[1024];
            uint size = strlen(DATA_INITIALIZATION);
            strcpy(tmp, data + size);
            char *p = strtok(tmp + size, "/");
            int i = 0;
            while (p)
            {
                switch (i++)
                {
                case 0:
                    timeTotal = atoi(p);
                    break;
                case 1:
                    timeFinal = atoi(p);
                    break;
                case 2:
                    fps = atoi(p);
                    break;
                case 3:
                    shutterClickSpeed = atoi(p);
                    break;
                case 4:
                    shutterSpeed = atoi(p);
                    break;
                }

                p = strtok(NULL, "/");
            }

            start = true;

            totalFrames = timeTotal * 60;
            secondsPerFrame = totalFrames / (timeFinal * fps);
            Serial.print(DATA_CONFIG);
            Serial.print(totalFrames);
            Serial.print("/");
            Serial.print(secondsPerFrame);
            Serial.print("/");
            Serial.println(timeTotal + ((shutterClickSpeed + shutterSpeed) / 60000.0 * totalFrames));
        }
    }
}

void SerialProcessData(byte data)
{
    //Serial Data Processing Credits - Nick Gammon
    static char input[MAX_SERIAL_CHARS];
    static unsigned int index = 0;

    switch (data)
    {
    case '\n':

        input[index] = 0;
        onSerialProcessData(input);
        index = 0;

        break;

    case '\r':
        break;

    default:
        if (index < (MAX_SERIAL_CHARS - 1))
            input[index++] = data;
        break;
    }
}

void loop()
{
    if (!start)
        while (Serial.available() > 0)
            SerialProcessData(Serial.read());
    else if (!ended)
    {
        if (frames < totalFrames)
        {
            digitalWrite(SHUTTER_PIN, 1);
            delay(10);
            digitalWrite(SHUTTER_PIN, 0);
            ++frames;
            delay(shutterSpeed);
            delay(shutterClickSpeed);
            delay(secondsPerFrame * 1000);
            Serial.println(frames);
        }
        else
        {
            Serial.println(DATA_END);
            ended = true;
        }
    }
}